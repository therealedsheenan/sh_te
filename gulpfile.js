const gulp = require('gulp')
const notify = require('gulp-notify')
const sass = require('gulp-sass')
const plumber = require('gulp-plumber')
const del = require('del')
const browserSync = require('browser-sync')
const imagemin = require('gulp-imagemin')
const autoprefixer = require('gulp-autoprefixer')

const server = browserSync.create()

function clean () {
  return del([ 'dist' ])
}

function images () {
  return gulp
    .src('./src/images/**/*.{jpg,jpeg,png}', {since: gulp.lastRun(images)})
    .pipe(imagemin({optimizationLevel: 5}))
    .pipe(gulp.dest('./dist/images/'))
}

function html () {
  return gulp
    .src('./src/*.html')
    .pipe(gulp.dest('./dist/'))
}

function styles () {
  return gulp.src('./src/styles/*.scss')
    .pipe(plumber({
      errorHandler: notify.onError('Error: <%= error.message %>')
    }))
    .pipe(sass())
    .pipe(autoprefixer({
			browsers: ['last 2 versions'],
			cascade: false
		}))
    .pipe(gulp.dest('./dist/styles'))
}

function reload (done) {
  server.reload()
  done()
}

function serve (done) {
  server.init({
    server: {
      baseDir: './dist'
    }
  })
  done()
}

const watch = () => {
  gulp.watch('./src/{*.hbs,*.html}', gulp.series(html, reload))
  gulp.watch('./src/html/{*.hbs,*.html}', gulp.series(html, reload))
  gulp.watch('./src/scripts/*.js', gulp.series(reload))
  gulp.watch('./src/styles/**/*.scss', gulp.series(styles, reload))
  gulp.watch('./src/images/**', gulp.series(images, reload))
}

const dev = gulp.series(clean, styles, html, serve, images, watch)

gulp.task('default', dev)

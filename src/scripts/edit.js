import { getDefaultValue } from './utils'

export default class Edit {
  getToolTips () {
    return document.getElementsByClassName('tooltip')
  }

  getToolTemplate (type, value = '') {
    return `
      <div class='tooltip'>
        <div class='input-group'>
          <input id='input-${type}' value='${value}'type='text' required>
          <span class='highlight'></span>
          <span class='bar'></span>
          <label>${type}</label>
        </div>
        <div class='tooltip__controls'>
          <a class='button button--primary button--controls' onclick='window.saveProfile(event, "${type}");'>Save</a>
          <a class='button button--secondary button--controls' onclick='window.editCancelDesktop(event);'>cancel</a>
        </div>
      </div>
    `
  }

  handleOnClickMobile (type) {
    const controls = document.getElementById('edit-controls')
    const info = document.getElementsByClassName('profile__content__info')
    const form = document.getElementsByClassName('profile__content__form')
    const editButton = document.getElementById('edit-button')
    controls.classList.toggle('is-hidden')
    info[0].classList.toggle('is-hidden')
    form[0].classList.toggle('is-hidden')
    editButton.classList.toggle('is-hidden')
  }

  handleFormSubmit () {
    const name = document.getElementById('mobile-input-name').value
    const website = document.getElementById('mobile-input-website').value
    const phone = document.getElementById('mobile-input-phone').value
    const location = document.getElementById('mobile-input-location').value
    window.localStorage.setItem('name', name)
    window.localStorage.setItem('website', website)
    window.localStorage.setItem('phone', phone)
    window.localStorage.setItem('location', location)
    window.location.reload()
  }

  // display tooltips
  handleOnShowToolTip (event, type) {
    this.removeAllToolTips()
    const parent = event.currentTarget.parentNode
    const getLocalValue = window.localStorage.getItem(type) ? window.localStorage.getItem(type) : getDefaultValue(type)
    parent.insertAdjacentHTML('afterbegin', this.getToolTemplate(type, getLocalValue))
  }

  handleRemoveToolTip (event) {
    event.currentTarget.parentNode.parentNode.remove()
  }

  handleSaveProfile (e, type) {
    const inputVal = e.currentTarget.parentNode.parentNode.querySelector(`#input-${type}`).value
    window.localStorage.setItem(type, inputVal)
    window.location.reload()
  }

  removeAllToolTips () {
    const toolTips = document.getElementsByClassName('tooltip')
    for (const tool of toolTips) {
      tool.remove()
    }
  }
}

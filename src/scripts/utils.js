import header from './templates/header'

export const addTitle = (title = '') => {
  const content = document.getElementById('content')
  content.insertAdjacentHTML('afterbegin', `<h1>${title}</h1>`)
}

export const addMenu = (url) => {
  const profile = document.getElementById('main')
  profile.insertAdjacentHTML('afterbegin', header(url))
}

export const getDefaultValue = (type) => {
  const defaults = {
    name: 'Jessica Parker',
    location: 'Newport Beach, CA',
    website: 'www.seller.com',
    phone: '(949) 325 - 68594'
  }

  if (type) {
    return defaults[type]
  }
  return ''
}

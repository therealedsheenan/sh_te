import { addTitle, addMenu } from './utils'

// render navigation
addMenu('settings')

// display content titles
addTitle('Settings')

import Edit from './edit'
import Tab from './tab'
import contents from './templates/about-contents'
import { addTitle, addMenu } from './utils'

// render navigation
addMenu('about')

// render tab contents
const content = document.getElementById('content')
content.insertAdjacentHTML('afterbegin', contents)

// events

window.profileFormSubmit = (e) => {
  const edit = new Edit()
  edit.handleFormSubmit()
}

window.profileFormCancel = (e) => {
  const edit = new Edit()
  edit.handleOnClickMobile()
}

window.tabClick = (e, tabName) => {
  const tab = new Tab()
  tab.handleTabClick(e, tabName)
}

window.editMobile = () => {
  const edit = new Edit()
  edit.handleOnClickMobile()
}

window.editDesktop = (e, type) => {
  const edit = new Edit()
  edit.handleOnShowToolTip(e, type)
}

window.saveProfile = (e, type) => {
  const edit = new Edit()
  edit.handleSaveProfile(e, type)
}

window.editCancelDesktop = (e) => {
  const edit = new Edit()
  edit.handleRemoveToolTip(e)
}

// display content titles
addTitle('About')

import { getDefaultValue } from '../utils'

export default `
  <div class="content__title">
    <button id="edit-button" class="button button__edit button--mobile" onclick="editMobile();">
      <i class="icon ion-md-create icon--gray"></i>
    </button>
    <div id="edit-controls" class="edit edit--mobile edit__controls is-hidden">
      <div>
        <button onclick="window.profileFormCancel(event)" class="button button--no-border">cancel</button>
        <button onclick="window.profileFormSubmit(event)" class="button button--no-border" >Save</button>
      </div>
    </div>
  </div>
  <div class="profile__content--tab is-shown" id="tab-about">
    <div class="profile__content profile__content__info">
      <ul>
        <li class="list list--clear">
          <div class="list__value">${window.localStorage.getItem('name') ? window.localStorage.getItem('name') : getDefaultValue('name')}</div>
          <button class="button button__edit button--desktop" onclick="window.editDesktop(event, 'name');">
            <i class="icon ion-md-create icon--gray"></i>
          </button>
        </li>
        <li class="list list--clear">
          <div class="list__icon">
            <i class="icon ion-md-globe icon--gray"></i>
          </div>
          <div class="list__value">${window.localStorage.getItem('website') ? window.localStorage.getItem('website') : getDefaultValue('website')}</div>
          <button class="button button__edit button--desktop" onclick="window.editDesktop(event, 'website');">
            <i class="icon ion-md-create icon--gray"></i>
          </button>
        </li>
        <li class="list list--clear">
          <div class="list__icon">
            <i class="icon ion-md-call icon--gray"></i>
          </div>
          <div class="list__value">${window.localStorage.getItem('phone') ? window.localStorage.getItem('phone') : getDefaultValue('phone')}</div>
          <button class="button button__edit button--desktop" onclick="window.editDesktop(event, 'phone');">
            <i class="icon ion-md-create icon--gray"></i>
          </button>
        </li>
        <li class="list list--clear">
          <div class="list__icon"><i class="icon ion-md-home icon--gray"></i></div>
          <div class="list__value">${window.localStorage.getItem('location') ? window.localStorage.getItem('location') : getDefaultValue('location')}</div>
          <button class="button button__edit button--desktop" onclick="window.editDesktop(event, 'location');">
            <i class="icon ion-md-create icon--gray"></i>
          </button>
        </li>
      </ul>
    </div>
    <div class="profile__content profile__content__form is-hidden">
      <div class="input-group">
        <input value="${window.localStorage.getItem('name') ? window.localStorage.getItem('name') : getDefaultValue('name')}" name="name" id="mobile-input-name"type="text">
        <span class="highlight"></span>
        <span class="bar"></span>
        <label>first name</label>
      </div>
      <div class="input-group">
        <input value="${window.localStorage.getItem('website') ? window.localStorage.getItem('website') : getDefaultValue('website')}" type="text" id="mobile-input-website">
        <span class="highlight"></span>
        <span class="bar"></span>
        <label>website</label>
      </div>
      <div class="input-group">
        <input value="${window.localStorage.getItem('phone') ? window.localStorage.getItem('phone') : getDefaultValue('phone')}" type="text" id="mobile-input-phone">
        <span class="highlight"></span>
        <span class="bar"></span>
        <label>phone number</label>
      </div>
      <div class="input-group">
        <input value="${window.localStorage.getItem('location') ? window.localStorage.getItem('location') : getDefaultValue('location')}" type="text" id="mobile-input-location">
        <span class="highlight"></span>
        <span class="bar"></span>
        <label>City, state & zip</label>
      </div>
      <div class="profile__content--tab" id="tab-settings">
        <div class="content__title">
          <h1>Settings</h1>
        </div>
      </div>
      <div class="profile__content--tab" id="tab-option1">
        <div class="content__title">
          <h1>Option 1</h1>
        </div>
      </div>
      <div class="profile__content--tab" id="tab-option2">
        <div class="content__title">
          <h1>Option 2</h1>
        </div>
      </div>

      <div class="profile__content--tab" id="tab-option3">
        <div class="content__title">
          <h1>Option 3</h1>
        </div>
      </div>
    </div>
  </div>
`

import { getDefaultValue } from '../utils'

export default (url) => {
  return `
    <header class="header profile__header">
      <button class="button button--disabled"><i class="icon ion-md-camera icon--gray"></i>Upload Cover Image</button>
      <div class="container">
        <div class="row">
          <div class="col--text-align-right">
            <button class="button button--no-border">Logout</button>
          </div>
        </div>
      </div>
      <figure class="figure figure--profile">
        <img src="/images/profile_image.jpg" alt="profile image"/>
        <figcaption>${window.localStorage.getItem('name') ? window.localStorage.getItem('name') : getDefaultValue('name')}</figcaption>
      </figure>
      <div class="profile__header__info">
        <div class="profile__header__top">
          <h3>${window.localStorage.getItem('name') ? window.localStorage.getItem('name') : getDefaultValue('name')}</h3>
          <ul>
            <li class="list">
              <div class="list__icon"><i class="icon ion-md-pin icon--gray"></i></div>
              <div class="list__value">${window.localStorage.getItem('location') ? window.localStorage.getItem('location') : getDefaultValue('location')}</div>
            </li>
            <li class="list">
              <div class="list__icon icon--gray"><i class="icon ion-md-call"></i></div>
              <div class="list__value">${window.localStorage.getItem('phone') ? window.localStorage.getItem('phone') : getDefaultValue('phone')}</div>
            </li>
          </ul>
          <figure class="figure figure--review">
            <div class="figure--review--image">
              <i class="icon ion-md-star icon--blue"></i>
              <i class="icon ion-md-star icon--blue"></i>
              <i class="icon ion-md-star icon--blue"></i>
              <i class="icon ion-md-star icon--blue"></i>
              <i class="icon ion-md-star icon--blue"></i>
            </div>
            <figcaption>6 reviews</figcaption>
          </figure>
        </div>
        <div class="profile__header__bottom">
          <figure class="figure figure--follower">
            <div class="figure--image">
              <i class="icon ion-md-add-circle icon--gray"></i>
            </div>
            <figcaption>15 Followers</figcaption>
          </figure>
          <nav class="nav nav--profile">
            <div class="container">
              <div class="nav__items-wrap">
                <a class="nav__item ${url === 'about' && ' nav__item--selected'}" href="./index.html">About</a>
                <a class="nav__item ${url === 'settings' && ' nav__item--selected'}" href="./settings.html">Settings</a>
                <a class="nav__item ${url === 'option1' && ' nav__item--selected'}" href="./option1.html">Option1</a>
                <a class="nav__item ${url === 'option2' && ' nav__item--selected'}" href="./option2.html">Option2</a>
                <a class="nav__item ${url === 'option3' && ' nav__item--selected'}" href="./option3.html">Option3</a>
              </div>
            </div>
          </nav>
        </div>
      </div>
    </header>
  `
}

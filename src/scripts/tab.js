export default class Tab {

  getTabContents () {
    return document.getElementsByClassName('profile__content--tab')
  }

  getNavItems () {
    return document.getElementsByClassName('nav__item')
  }

  handleTabClick (event, tabName) {
    const tabs = this.getTabContents()
    const navItems = this.getNavItems()
    for (const tab of tabs) {
      tab.classList.remove('is-shown')
    }
    for (const item of navItems) {
      item.classList.remove('nav__item--selected')
    }

    // add styles to tab contents
    document.getElementById(`tab-${tabName}`).classList.add('is-shown')
    // add styles to navigation item
    event.currentTarget.classList.add('nav__item--selected')
  }
}

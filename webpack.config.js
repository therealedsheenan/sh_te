module.exports = {
  entry: {
    about: './src/scripts/about.js',
    settings: './src/scripts/settings.js',
    option1: './src/scripts/option1.js',
    option2: './src/scripts/option2.js',
    option3: './src/scripts/option3.js'
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      }
    ]
  },
  watch: true,
  resolve: {
    extensions: ['*', '.js', '.jsx']
  },
  output: {
    path: __dirname + '/dist/scripts',
    publicPath: '/',
    filename: "[name].js"
  }
}
